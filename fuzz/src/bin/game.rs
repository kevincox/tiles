#![no_main]

use libfuzzer_sys::fuzz_target;
use rand::Rng;
use std::io::Read;

struct SharedRead<R>(std::rc::Rc<std::cell::RefCell<R>>);

impl<R: std::io::Read> std::io::Read for SharedRead<R> {
	fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
		self.0.borrow_mut().read(buf)
	}
}

fuzz_target!(|data: &[u8]| {
	let input = std::io::Cursor::new(data).chain(std::io::repeat(0));
	let rc = std::rc::Rc::new(std::cell::RefCell::new(input));

	let mut rng = rand::rngs::adapter::ReadRng::new(SharedRead(rc.clone()));
	let mut game = tiles::Game::new(tiles::Config{
		players: rng.gen_range(1, 100),
		use_preset: rng.gen(),
		rng
	});

	let mut rng = rand::rngs::adapter::ReadRng::new(SharedRead(rc));

	for _ in 0..100 {
		let byte: u8 = rng.gen();
		let path = match byte % 3 {
			0 => tiles::TilePath::Select(tiles::TileSource{
				source: rng.gen_range(0, game.sources.len() as u8),
				kind: rng.gen(),
			}),
			1 => tiles::TilePath::Queue(
				tiles::TileQueue{
					player: rng.gen_range(0, game.players.len() as u8),
					row: rng.gen_range(0, 5),
				},
				rng.gen()),
			2 => tiles::TilePath::Score(
				tiles::TileBoard{
					player: rng.gen_range(0, game.players.len() as u8),
					row: rng.gen_range(0, 5),
					col: rng.gen_range(0, 5),
				}),
			b => unreachable!("r: {}", b),
		};
		game.click(path);
		game.mutations.clear();
	}
});
