{
	nixpkgs ? import <nixpkgs> {},
	ecl ? builtins.storePath (nixpkgs.lib.fileContents (builtins.fetchurl "https://kevincox.gitlab.io/ecl/ecl.nixpath")),
	naersk ? nixpkgs.pkgs.callPackage (fetchTarball "https://github.com/nix-community/naersk/archive/master.tar.gz") {
		rustc = nixpkgs.pkgs.rustc-wasm32;
	},
	npmlock2nix ? nixpkgs.pkgs.callPackage (
		fetchTarball "https://github.com/nix-community/npmlock2nix/archive/refs/heads/master.tar.gz"
	) {},

	api-url ? let
		commit = nixpkgs.lib.maybeEnv "CI_COMMIT_SHA" null;
		url = if commit == null
			then "ws://localhost:8080"
			else "wss://${commit}.api.tiles.kevincox.ca";
		in url,
}: let
	rust-root = nixpkgs.pkgs.nix-gitignore.gitignoreSource [
		"*"
		"!Cargo.*"
		"!src/"
	] ./.;
	web-src = nixpkgs.pkgs.nix-gitignore.gitignoreSource [
		"*.ecl"
		"*.md"
		"*.nix"
		".*"
		"Cargo.*"
		"src/"
	] ./.;
in rec {
	server = naersk.buildPackage {
		name = "tiles-api";
		root = rust-root;
		buildInputs = with nixpkgs.pkgs; [ pkg-config ];
		doCheck = true;
	};

	wasm = naersk.buildPackage {
		name = "tiles-client";
		root = rust-root;
		buildInputs = with nixpkgs.pkgs; [ lld ];
		RUSTFLAGS = "-C linker=lld";
		cargoBuildOptions = f: f ++ ["--target=wasm32-unknown-unknown" "--lib"];
		copyLibs = true;
	};

	wasm-bindgen = nixpkgs.pkgs.runCommand "tiles-wasm-bindgen" {} ''
		${nixpkgs.pkgs.wasm-bindgen-cli}/bin/wasm-bindgen ${wasm}/lib/tiles.wasm --target=web --out-name=tiles --out-dir=$out
		# ${nixpkgs.pkgs.binaryen}/bin/wasm-opt $out/tiles_bg.wasm -o $out/tiles_bg.wasm -O4
	'';

	web-noapi = npmlock2nix.v2.build {
		src = web-src;

		nodejs = nixpkgs.pkgs.nodejs;

		buildCommands = [''
			ln -s ${wasm-bindgen} wasm
			parcel build \
				--public-url=. \
				*.pug
		''];

		installPhase = ''
			mv dist "$out"
			cp ping.txt "$out"
		'';
	};

	web = nixpkgs.pkgs.runCommand "web" {} ''
		cp -rv --no-preserve=mode ${web-noapi} $out
		sed 's_ws://78c45a36-0240-4b9e-8d78-4de97ee4a441.example:8080_${api-url}_' ${web-noapi}/index.html >$out/index.html
	'';

	web-zip = nixpkgs.pkgs.runCommand "tiles-web.zip" {} ''
		cd ${web}
		${nixpkgs.pkgs.zip}/bin/zip -rv - . >$out
	'';

	docker = nixpkgs.dockerTools.buildLayeredImage {
		name = "tiles-api";
		tag = "latest";
		config = {
			Cmd = [ "${server}/bin/tiles" ];
			Env = [
				"RUST_BACKTRACE=full"
				"TILES_BIND=0.0.0.0:8080"
			];
			ExposedPorts = { "8080/tcp" = {}; };
		};
	};

	kube = nixpkgs.pkgs.runCommand "kube.json" {
		CI_COMMIT_REF_SLUG = nixpkgs.lib.maybeEnv "CI_COMMIT_REF_SLUG" null;
		CI_COMMIT_SHA = nixpkgs.lib.maybeEnv "CI_COMMIT_SHA" null;
		CI_ENVIRONMENT_SLUG = nixpkgs.lib.maybeEnv "CI_ENVIRONMENT_SLUG" null;
		CI_PROJECT_PATH_SLUG = nixpkgs.lib.maybeEnv "CI_PROJECT_PATH_SLUG" null;
	} ''
		${ecl}/bin/ecl load ${./kube.ecl} >$out
	'';
}
