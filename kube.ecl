{
	environment = env:"CI_COMMIT_REF_SLUG" || "local"
	version = env:"CI_COMMIT_SHA" || "latest"
	name-version = "tiles-api-{version}"

	env-selector = {
		app-env = "tiles-api-{environment}"
	}
	version-selector = {
		app-ver = name-version
	}

	common-metadata = {
		namespace = "tiles-api"
		labels = env-selector:version-selector
	}
	env-metadata = common-metadata:{ name = "tiles-api-{environment}" }
	version-metadata = common-metadata:{ name = "tiles-api-{version}" }

	ingress = ->name {
		apiVersion = "networking.k8s.io/v1"
		kind = "Ingress"
		metadata = common-metadata:{
			name = "tiles-api-{...name}"
			annotations."cert-manager.io/cluster-issuer" = "letsencrypt"
		}
		spec = {
			tls = [{
				hosts = [
					"api-tiles.kevincox.ca"
					"*.api.tiles.kevincox.ca"
				]
				secretName = "tls"
			}]
			rules = [{
				host = "{name}.api.tiles.kevincox.ca"
				http.paths = [{
					path = "/"
					pathType = "Prefix"
					backend.service = {
						name = name-version
						port.number = 80
					}
				}]
			}]
		}
	}

	r = {
		apiVersion = "v1"
		kind = "List"
		metadata = {}

		items = [{
			apiVersion = "v1"
			kind = "Namespace"
			metadata.name = common-metadata.namespace
		} {
			apiVersion = "apps/v1"
			kind = "Deployment"
			metadata = version-metadata
			spec = {
				replicas = 1
				selector.matchLabels = env-selector
				template = {
					metadata = common-metadata
					spec.containers = [{
						name = "tiles-api"
						image = "registry.gitlab.com/kevincox/tiles:{version}"
						ports = [{containerPort = 8080}]
					}]
					spec.imagePullSecrets = [{name = "tiles-registry-creds"}]
				}
			}
		} {
			apiVersion = "v1"
			kind = "Service"
			metadata = version-metadata
			spec = {
				ports = [{port = 80 targetPort = 8080}]
				selector = version-selector
			}
		}]
		+ [ingress:version]
		+ [ingress:environment:{metadata.labels."kevincox.ca/is-env"="true"}]
	}
}.r
