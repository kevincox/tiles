use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern {
	#[wasm_bindgen(js_namespace = window)]
	fn onerror(err: &JsValue);

	type Error;

	#[wasm_bindgen(constructor)]
	fn new() -> Error;

	#[wasm_bindgen(structural, method, getter)]
	fn stack(error: &Error) -> String;
}

#[wasm_bindgen]
#[allow(unused)]
pub fn set_panic_hook() {
	#[derive(serde::Serialize)]
	struct FakeError {
		filename: String,
		lineno: u32,
		message: String,
		stack: String,
	}

	std::panic::set_hook(Box::new(|info| {
		let mut err = FakeError{
			filename: "rust".into(),
			lineno: 0,
			message: format!("{}", info),
			stack: Error::new().stack(),
		};
		if let Some(location) = info.location() {
			err.filename = location.file().into();
			err.lineno = location.line();
		}
		onerror(&JsValue::from_serde(&err).unwrap());
	}));
}
