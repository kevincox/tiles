use rand::Rng;

#[derive(Copy,Clone,Debug,Eq,PartialEq,serde_derive::Serialize,serde_derive::Deserialize)]
pub enum Tile {
	Red,
	Blue,
	Green,
	Black,
	White,
}

impl Tile {
	pub fn id(&self) -> &'static str {
		match self {
			Tile::Red => "Red",
			Tile::Blue => "Blue",
			Tile::Green => "Green",
			Tile::Black => "Black",
			Tile::White => "White",
		}
	}
}

impl<T: Into<TileSet>> std::ops::Add<T> for Tile {
	type Output = TileSet;

	fn add(self, other: T) -> TileSet {
		other.into() + self
	}
}

impl std::ops::Mul<u8> for Tile {
	type Output = Tiles;

	fn mul(self, count: u8) -> Tiles {
		Tiles::new(self, count).unwrap()
	}
}

impl std::ops::Mul<Tile> for u8 {
	type Output = Tiles;

	fn mul(self, tile: Tile) -> Tiles {
		tile * self
	}
}

pub const TILES: &[Tile] = &[
	Tile::Red,
	Tile::Blue,
	Tile::Green,
	Tile::Black,
	Tile::White,
];

impl rand::distributions::Distribution<Tile> for rand::distributions::Standard {
	fn sample<R: rand::Rng + ?Sized>(&self, rng: &mut R) -> Tile {
		*rand::seq::SliceRandom::choose(TILES, rng).unwrap()
	}
}

#[derive(Clone,Debug,Eq,serde_derive::Serialize,serde_derive::Deserialize)]
pub struct Tiles {
	kind: Tile,
	count: std::num::NonZeroU8,
}

impl Tiles {
	pub fn new(kind: Tile, count: u8) -> Option<Self> {
		std::num::NonZeroU8::new(count).map(|count| Tiles {
			kind,
			count,
		})
	}

	pub fn kind(&self) -> Tile {
		self.kind
	}

	pub fn len(&self) -> u8 {
		self.count.get()
	}

	pub fn iter(&self) -> impl Iterator<Item=Tile> {
		let kind = self.kind;
		(0..self.len()).map(move |_| kind)
	}
}

impl PartialEq for Tiles {
	fn eq(&self, other: &Tiles) -> bool {
		self.count == other.count && self.kind() == other.kind()
	}
}

impl<T: Into<Tile>> From<T> for Tiles {
	fn from(t: T) -> Tiles {
		t.into() * 1
	}
}

impl<T: Into<TileSet>> std::ops::Add<T> for Tiles {
	type Output = TileSet;

	fn add(self, other: T) -> TileSet {
		other.into() + self
	}
}

#[derive(Default,Clone,PartialEq,serde_derive::Serialize,serde_derive::Deserialize)]
pub struct TileSet([u8; 5]);

impl TileSet {
	pub fn is_empty(&self) -> bool {
		self.iter().all(|tiles| tiles.is_none())
	}

	pub fn clear(&mut self) {
		*self = Default::default();
	}

	fn randomize(&mut self, rng: &mut impl rand::Rng) {
		rng.sample_iter::<Tile, _>(rand::distributions::Standard)
			.take(4)
			.for_each(|tile| self[tile] += 1);
	}

	pub fn iter(&self) -> impl Iterator<Item=Option<Tiles>> {
		let this = self.clone();
		TILES.iter().map(move |&tile| Tiles::new(tile, this[tile]))
	}

	pub fn iter_non_empty(&self) -> impl Iterator<Item=Tiles> {
		self.iter().flatten()
	}
}

impl<T: Into<Tiles>> From<T> for TileSet {
	fn from(t: T) -> TileSet {
		let tiles = t.into();
		let mut set = TileSet::default();
		set[tiles.kind] = tiles.len();
		set
	}
}

impl<T: Into<TileSet>> std::ops::Add<T> for TileSet {
	type Output = TileSet;

	fn add(mut self, other: T) -> TileSet {
		self += other;
		self
	}
}

impl<T: Into<TileSet>> std::ops::AddAssign<T> for TileSet {
	fn add_assign(&mut self, other: T) {
		other.into().iter_non_empty().for_each(|tiles| self[tiles.kind] += tiles.len());
	}
}

impl std::fmt::Debug for TileSet {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		f.debug_map()
			.entries(self.iter_non_empty().map(|t| (t.kind, t.count)))
			.finish()
	}
}

impl std::ops::Index<Tile> for TileSet {
	type Output = u8;

	fn index(&self, t: Tile) -> &u8 {
		&self.0[t as usize]
	}
}

impl std::ops::IndexMut<Tile> for TileSet {
	fn index_mut(&mut self, t: Tile) -> &mut u8 {
		&mut self.0[t as usize]
	}
}

pub fn queue_size(col: u8) -> u8 {
	col + 1
}

pub const ROWS: &[u8] = &[0, 1, 2, 3, 4];

fn default_preset() -> [[Tile; 5]; 5] {
	let mut board = [[Tile::Red; 5]; 5];
	for (y, row) in board.iter_mut().enumerate() {
		for (x, col) in row.iter_mut().enumerate() {
			*col = TILES[(x + y) % TILES.len()];
		}
	}
	board
}

#[derive(Clone,Debug,Default,serde_derive::Deserialize,serde_derive::Serialize)]
pub struct Player {
	pub preset: Option<[[Tile; 5]; 5]>,
	pub board: [[Option<Tile>; 5]; 5],
	pub queue: [Option<Tiles>; 5],
	pub score: u32,
	pub trash: u8,
}

impl Player {
	fn new() -> Self {
		Default::default()
	}

	pub fn board_row(&self, row: u8) -> &[Option<Tile>; 5] {
		&self.board[row as usize]
	}

	pub fn queue_row(&self, row: u8) -> &Option<Tiles> {
		&self.queue[row as usize]
	}

	pub fn can_score_tile<'a>(&'a self, row: u8, kind: Tile) -> Box<dyn Iterator<Item=u8> + 'a> {
		if let Some(preset) = &self.preset {
			return Box::new(
				preset[row as usize].iter()
					.enumerate()
					.filter(move |(_, tile)| **tile == kind)
					.map(|(i, _)| i as u8))
		}

		let row = self.board_row(row);
		let exists_in_row = row.iter().any(|&cell| cell == Some(kind));
		if exists_in_row {
			return Box::new(std::iter::empty())
		}

		Box::new(row.iter()
			.enumerate()
			.filter(|(_, cell)| cell.is_none())
			.filter(move |(col, _)|
				self.board.iter()
					.map(|row| row[*col as usize])
					.all(|col| col != Some(kind)))
			.map(|(col, _)| col as u8))
	}

	fn can_score(&self) -> Option<u8> {
		self.queue.iter()
			.enumerate()
			.flat_map(|(i, queue)| queue.as_ref().map(|q| (i as u8, q)))
			.find(|(row, queue)| queue.len() == queue_size(*row))
			.map(|(row, _)| row)
	}

	fn tile_score(&self, row: u8, col: u8) -> u32 {
		// Assumes tile is valid.

		let row = row as usize;
		let col = col as usize;

		let width = self.board[0].len();
		let height = self.board.len();

		let up = (0..row).rev().take_while(|&y| self.board[y][col].is_some()).count();
		let down = (row+1..width).take_while(|&y| self.board[y][col].is_some()).count();
		let left = (0..col).rev().take_while(|&x| self.board[row][x].is_some()).count();
		let right = (col+1..height).take_while(|&x| self.board[row][x].is_some()).count();

		let vertial = up + down;
		let horizontal = left + right;

		let mut total = 1 + vertial + horizontal;
		if vertial > 0 && horizontal > 0 {
			total += 1;
		}

		total as u32
	}

	fn trash_cost(&self) -> u32 {
		/* The cost of each trashed tile goes up by 1 every 2.
			N	Cost Cumulative
			1	1	1
			2	1	2
			3	2	4
			4	2	6
			5	3	9
			6	3	12
		*/

		(0..self.trash as u32).map(|n| n / 2 + 1).sum()
	}
}

#[derive(Clone,Debug,PartialEq,serde_derive::Serialize,serde_derive::Deserialize)]
pub struct TileSource {
	pub source: u8,
	pub kind: Tile,
}

#[derive(Clone,Debug,PartialEq,serde_derive::Serialize,serde_derive::Deserialize)]
pub struct TileQueue {
	pub player: u8,
	pub row: u8,
}

#[derive(Clone,Debug,PartialEq,serde_derive::Serialize,serde_derive::Deserialize)]
pub struct TileBoard {
	pub player: u8,
	pub row: u8,
	pub col: u8,
}

#[derive(Clone,Debug,PartialEq,serde_derive::Serialize,serde_derive::Deserialize)]
pub enum TilePath {
	Select(TileSource),
	Queue(TileQueue, u8),
	Score(TileBoard),
}

#[derive(Clone,Debug,PartialEq,serde_derive::Serialize,serde_derive::Deserialize)]
pub struct Queue {
	pub row: u8,
	pub tiles: Tiles,
}

#[derive(Clone,Debug,PartialEq,serde_derive::Serialize,serde_derive::Deserialize)]
pub enum Mutation {
	Ok,
	Err(String),
	NewRound {
		sources: Vec<TileSet>,
		scores: Vec<u32>,
	},
	Select {
		source: u8,
		tiles: Tiles,
	},
	Place {
		player: u8,
		source: u8,
		kind: Tile,
		queue: Option<Queue>,
		trash: u8,
	},
	Score {
		queue: TileQueue,
		col: u8,
		score: u32,
	}
}

#[derive(Clone,Debug,PartialEq,serde_derive::Deserialize,serde_derive::Serialize)]
pub enum State {
	Selecting,
	Queuing{source: u8, kind: Tile},
	Scoring,
}

#[derive(Clone,Debug,serde_derive::Deserialize,serde_derive::Serialize)]
pub struct Config<R> {
	pub players: u8,
	pub use_preset: bool,
	pub rng: R,
}

impl Default for Config<rand_pcg::Pcg32> {
	fn default() -> Self {
		Config{
			players: 2,
			use_preset: false,
			rng: rand::SeedableRng::from_entropy(),
		}
	}
}

#[derive(Clone,Debug,serde_derive::Deserialize,serde_derive::Serialize)]
pub struct Game<R = crate::Prng> {
	rng: R,

	pub players: Vec<Player>,
	pub sources: Vec<TileSet>,

	pub first_player: Option<u8>,
	pub current_player: u8,

	pub state: State,

	#[serde(skip)]
	pub mutations: Vec<Mutation>,
}

impl<R: rand::RngCore> Game<R> {
	pub fn new(config: Config<R>) -> Self {
		let Config{players, use_preset, rng} = config;

		let sources = (0..players+4).map(|_| Default::default()).collect();
		let players = (0..players).map(|_| {
			let mut p = Player::new();
			if use_preset {
				p.preset = Some(default_preset());
			}
			p
		}).collect();

		let mut game = Game {
			rng,

			players,
			sources,

			first_player: None,
			current_player: 0,

			state: State::Selecting,
			mutations: Vec::new(),
		};
		game.start_round();
		game.mutations.clear();
		game
	}

	fn advance_player(&mut self) {
		self.current_player += 1;
		self.current_player %= self.players.len() as u8;
	}

	fn start_round(&mut self) {
		let mut sources = std::mem::replace(&mut self.sources, Vec::new());
		sources[0].clear();
		for source in &mut sources[1..] {
			debug_assert_eq!(*source, Default::default());
			source.randomize(&mut self.rng);
		}

		let scores = self.players.iter()
			.map(|p| p.score.saturating_sub(p.trash_cost()))
			.collect();

		self.mutate(Mutation::NewRound{
			sources,
			scores,
		});
	}

	fn continue_scoring(&mut self) {
		let start = self.current_player;

		loop {
			let player = &mut self.players[self.current_player as usize];
			let can_score = player.can_score();
			if can_score.is_some() {
				self.state = State::Scoring;
				return
			}

			self.advance_player();
			if self.current_player == start {
				return self.start_round()
			}
		}
	}

	pub fn click(&mut self, target: TilePath) {
		match (self.state.clone(), target) {
			(State::Selecting, TilePath::Select(from))
			| (State::Queuing{..}, TilePath::Select(from)) => {
				let source = &self.sources[from.source as usize];
				let tiles = Tiles::new(from.kind, source[from.kind]);
				if let Some(tiles) = tiles {
					self.mutate(Mutation::Select { source: from.source, tiles });
				} else {
					self.mutate(Mutation::Err("Ignoring empty source".into()));
					return
				}
			}
			(
				State::Queuing{source: source_id, kind},
				TilePath::Queue(TileQueue{player: player_id, row}, _col))
			=> {
				if player_id != self.current_player {
					self.mutate(Mutation::Err("Ignoring out-of-turn player".into()));
					return
				};

				let source = &self.sources[source_id as usize];
				let count = source[kind];
				if count == 0 {
					let msg = format!("Expected tile {:?} not found. Got {:?}", kind, source);
					self.mutate(Mutation::Err(msg));
					return
				}

				let player = &self.players[player_id as usize];
				let queue = player.queue_row(row);

				let previous = queue.as_ref().map_or(0, |t| t.len());

				let can_score = player.can_score_tile(row, kind).next().is_some();
				let compatible_color = queue.is_none() || queue.as_ref().map(|t| t.kind()) == Some(kind);
				let can_merge = compatible_color && previous < queue_size(row);


				let accept = can_score && can_merge;
				let accepted = if accept { count } else { 0 };

				let total = previous + accepted;
				let new = total.min(queue_size(row));

				let queue_mutation = if accept {
					Some(Queue{
						row,
						tiles: Tiles::new(kind, new).unwrap(),
					})
				} else {
					None
				};

				let new_trash = previous + count - new;
				let mut trash = player.trash + new_trash;

				if source_id == 0 && self.first_player.is_none() {
					trash += 1;
				}

				self.mutate(Mutation::Place {
					player: player_id,
					source: source_id,
					kind,
					queue: queue_mutation,
					trash,
				});
			}
			(State::Scoring, TilePath::Score(TileBoard{player: player_id, row, col})) => {
				let player = &self.players[player_id as usize];

				let can_score = player.can_score();
				if can_score != Some(row) {
					self.mutate(Mutation::Err(format!(
						"Ignoring unscorable row {:?} expected {:?}",
						row, can_score)));
					return
				}

				let kind = player.queue_row(row).as_ref().unwrap().kind;
				let is_valid = player.can_score_tile(row, kind)
					.any(|valid_col| valid_col == col);
				if !is_valid {
					let msg = format!(
						"Ignoring invalid column valid are {:?}",
						player.can_score_tile(row, kind).collect::<Vec<_>>());
					self.mutate(Mutation::Err(msg));
					return
				}

				if player.board_row(row)[col as usize].is_some() {
					self.mutate(Mutation::Err("Ignoring taken cell.".into()));
					return
				}

				let mut score = player.tile_score(row, col);
				score += player.score;

				self.mutate(Mutation::Score {
					queue: TileQueue{player: player_id, row},
					col,
					score,
				});
			}
			_ => self.mutate(Mutation::Err(format!("Ignoring event in state {:?}", self.state))),
		}

		self.mutate(Mutation::Ok);
	}

	pub fn mutate(&mut self, mutation: Mutation) {
		self.mutations.push(mutation.clone());
		match mutation {
			Mutation::Ok => {}
			Mutation::Err(_) => {}
			Mutation::NewRound{sources, scores} => {
				for (player, score) in self.players.iter_mut().zip(scores) {
					player.score = score;
					player.trash = 0;
				}
				self.state = State::Selecting;
				self.current_player = self.first_player.take().unwrap_or(0);
				self.sources = sources;
			}
			Mutation::Select{source, tiles} => {
				self.state = State::Queuing{source, kind: tiles.kind};
			}
			Mutation::Place{player: player_id, source: source_id, kind, queue, trash} => {
				let source = &mut self.sources[source_id as usize];
				source[kind] = 0;
				if source_id == 0 {
					if self.first_player.is_none() {
						self.first_player = Some(player_id);
					}
				} else {
					let tiles = source.clone();
					source.clear();
					self.sources[0] += tiles;
				}

				let player = &mut self.players[player_id as usize];
				player.trash = trash;
				if let Some(Queue{row, tiles}) = queue {
					player.queue[row as usize] = Some(tiles);
				}

				self.advance_player();

				let done = self.sources.iter().all(|s| s.is_empty());
				if done {
					self.continue_scoring();
				} else {
					self.state = State::Selecting;
				};
			}
			Mutation::Score{queue: queue_id, col, score} => {
				let player = &mut self.players[queue_id.player as usize];
				player.score = score;

				let queue = &mut player.queue[queue_id.row as usize];
				player.board[queue_id.row as usize][col as usize] = Some(queue.as_ref().unwrap().kind);
				*queue = None;

				self.continue_scoring();
			}
		}
	}
}

#[cfg(test)]
fn parse_board(s: &str) -> [[Option<Tile>; 5]; 5] {
	let mut board = [[None; 5]; 5];
	for (y, line) in s.trim().lines().enumerate() {
		for (x, c) in line.trim().chars().enumerate() {
			board[y][x] = match c {
				'R' => Some(Tile::Red),
				'U' => Some(Tile::Blue),
				'G' => Some(Tile::Green),
				'B' => Some(Tile::Black),
				'W' => Some(Tile::White),
				'.' => None,
				c => unreachable!("Invalid tile {:?}", c),
			};
		}
	}
	board
}

#[test]
fn test_play() {
	use rand::SeedableRng;

	let mut game = Game::new(Config{
		players: 2,
		use_preset: false,
		rng: rand_pcg::Pcg64Mcg::seed_from_u64(0),
	});
	assert_eq!(game.mutations, vec![]);

	let mut tile_set = TileSet::default();

	game.click(TilePath::Select(TileSource{source: 3, kind: Tile::Green}));
	assert_eq!(game.sources[0], tile_set);
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Select {
			source: 3,
			tiles: Tile::Green * 3,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Queue(TileQueue{player: 0, row: 0}, 0));
	tile_set[Tile::Black] += 1;
	assert_eq!(game.sources[0], tile_set);
	assert_eq!(game.sources[3], Default::default());
	assert_eq!(game.players[0].queue[0], Tiles::new(Tile::Green, 1));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Place {
			player: 0,
			source: 3,
			kind: Tile::Green,
			queue: Some(Queue{
				row: 0,
				tiles: Tile::Green*1,
			}),
			trash: 2,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Select(TileSource{source: 2, kind: Tile::White}));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Select {
			source: 2,
			tiles: Tile::White * 1,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Queue(TileQueue{player: 1, row: 4}, 0));
	assert_eq!(game.sources[2], Default::default());
	assert_eq!(game.sources[3], Default::default());
	tile_set[Tile::Red] += 1;
	tile_set[Tile::Blue] += 1;
	tile_set[Tile::Green] += 1;
	assert_eq!(game.sources[0], tile_set);
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Place {
			player: 1,
			source: 2,
			kind: Tile::White,
			queue: Some(Queue{
				row: 4,
				tiles: Tile::White*1,
			}),
			trash: 0,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Select(TileSource{source: 4, kind: Tile::Black}));
	assert_eq!(game.sources[2], Default::default());
	assert_eq!(game.sources[3], Default::default());
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Select {
			source: 4,
			tiles: Tile::Black * 1,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Queue(TileQueue{player: 0, row: 2}, 0));
	tile_set[Tile::Red] += 1;
	tile_set[Tile::Green] += 2;
	assert_eq!(game.sources[0], tile_set);
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Place {
			player: 0,
			source: 4,
			kind: Tile::Black,
			queue: Some(Queue{
				row: 2,
				tiles: Tile::Black*1,
			}),
			trash: 2,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Select(TileSource{source: 1, kind: Tile::Green}));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Select {
			source: 1,
			tiles: Tile::Green * 1,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Queue(TileQueue{player: 1, row: 4}, 0));
	tile_set[Tile::Red] += 1;
	tile_set[Tile::Blue] += 1;
	tile_set[Tile::White] += 1;
	assert_eq!(game.sources[0], tile_set);
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Place {
			player: 1,
			source: 1,
			kind: Tile::Green,
			queue: None,
			trash: 1,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Select(TileSource{source: 5, kind: Tile::White}));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Select {
			source: 5,
			tiles: Tile::White * 2,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Queue(TileQueue{player: 0, row: 1}, 0));
	tile_set[Tile::Red] += 1;
	tile_set[Tile::Green] += 1;
	assert_eq!(game.sources[0], tile_set);
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Place {
			player: 0,
			source: 5,
			kind: Tile::White,
			queue: Some(Queue{
				row: 1,
				tiles: Tile::White*2,
			}),
			trash: 2,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Select(TileSource{source: 0, kind: Tile::White}));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Select {
			source: 0,
			tiles: Tile::White * 1,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Queue(TileQueue{player: 1, row: 3}, 0));
	tile_set[Tile::White] = 0;
	assert_eq!(game.sources[0], tile_set);
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Place {
			player: 1,
			source: 0,
			kind: Tile::White,
			queue: Some(Queue{
				row: 3,
				tiles: Tile::White*1,
			}),
			trash: 2,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Select(TileSource{source: 0, kind: Tile::Green}));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Select {
			source: 0,
			tiles: Tile::Green * 4,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Queue(TileQueue{player: 0, row: 3}, 0));
	tile_set[Tile::Green] = 0;
	assert_eq!(game.sources[0], tile_set);
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Place {
			player: 0,
			source: 0,
			kind: Tile::Green,
			queue: Some(Queue{
				row: 3,
				tiles: Tile::Green*4,
			}),
			trash: 2,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Select(TileSource{source: 0, kind: Tile::Red}));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Select {
			source: 0,
			tiles: Tile::Red * 4,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Queue(TileQueue{player: 1, row: 2}, 0));
	tile_set[Tile::Red] = 0;
	assert_eq!(game.sources[0], tile_set);
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Place {
			player: 1,
			source: 0,
			kind: Tile::Red,
			queue: Some(Queue{
				row: 2,
				tiles: Tile::Red*3,
			}),
			trash: 3,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Select(TileSource{source: 0, kind: Tile::Blue}));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Select {
			source: 0,
			tiles: Tile::Blue * 2,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Queue(TileQueue{player: 0, row: 4}, 0));
	tile_set[Tile::Blue] = 0;
	assert_eq!(game.sources[0], tile_set);
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Place {
			player: 0,
			source: 0,
			kind: Tile::Blue,
			queue: Some(Queue{
				row: 4,
				tiles: Tile::Blue*2,
			}),
			trash: 2,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Select(TileSource{source: 0, kind: Tile::Black}));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Select {
			source: 0,
			tiles: Tile::Black * 1,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Queue(TileQueue{player: 1, row: 0}, 0));
	assert_eq!(game.sources[0], Default::default());
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Place {
			player: 1,
			source: 0,
			kind: Tile::Black,
			queue: Some(Queue{
				row: 0,
				tiles: Tile::Black*1,
			}),
			trash: 3,
		},
		Mutation::Ok,
	]);

	assert_eq!(game.state, State::Scoring);

	game.click(TilePath::Score(TileBoard{player: 0, row: 0, col: 0}));
	assert!(game.players[0].queue[0].is_none());
	assert_eq!(game.players[0].board[0][0], Some(Tile::Green));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Score {
			queue: TileQueue{player: 0, row: 0},
			col: 0,
			score: 1,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Score(TileBoard{player: 0, row: 1, col: 0}));
	assert!(game.players[0].queue[1].is_none());
	assert_eq!(game.players[0].board[1][0], Some(Tile::White));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Score {
			queue: TileQueue{player: 0, row: 1},
			col: 0,
			score: 3,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Score(TileBoard{player: 0, row: 3, col: 0}));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Err("Ignoring invalid column valid are [1, 2, 3, 4]".into()),
	]);

	game.click(TilePath::Score(TileBoard{player: 0, row: 3, col: 1}));
	assert!(game.players[0].queue[3].is_none());
	assert_eq!(game.players[0].board[3][1], Some(Tile::Green));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Score {
			queue: TileQueue{player: 0, row: 3},
			col: 1,
			score: 4,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Score(TileBoard{player: 1, row: 0, col: 1}));
	assert!(game.players[1].queue[0].is_none());
	assert_eq!(game.players[1].board[0][1], Some(Tile::Black));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Score {
			queue: TileQueue{player: 1, row: 0},
			col: 1,
			score: 1,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Score(TileBoard{player: 1, row: 2, col: 4}));
	assert!(game.players[1].queue[2].is_none());
	assert_eq!(game.players[1].board[2][4], Some(Tile::Red));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Score {
			queue: TileQueue{player: 1, row: 2},
			col: 4,
			score: 2,
		},
		Mutation::NewRound {
			sources: vec![
				TileSet::default(),
				Tile::Blue + Tile::Green + 2*Tile::Black,
				2*Tile::Red + Tile::Blue + Tile::White,
				Tile::Red*2 + Tile::Black + Tile::White,
				(4*Tile::Black).into(),
				Tile::Red + 2*Tile::Blue + Tile::Green,
			],
			scores: vec![2, 0],
		},
		Mutation::Ok,
	]);

	assert_eq!(game.state, State::Selecting);
	assert!(!game.sources[1].is_empty());
	assert!(!game.sources[2].is_empty());
	assert!(game.first_player.is_none());
	assert_eq!(game.current_player, 1);
}

#[test]
fn test_trash_over() {
	use rand::SeedableRng;

	let mut game = Game::new(Config{
		players: 1,
		use_preset: false,
		rng: rand_pcg::Pcg64Mcg::seed_from_u64(0),
	});
	assert_eq!(game.mutations, vec![]);

	game.click(TilePath::Select(TileSource{source: 3, kind: Tile::Green}));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Select {
			source: 3,
			tiles: Tile::Green * 3,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Queue(TileQueue{player: 0, row: 0}, 0));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Place {
			player: 0,
			source: 3,
			kind: Tile::Green,
			queue: Some(Queue{
				row: 0,
				tiles: Tile::Green*1,
			}),
			trash: 2,
		},
		Mutation::Ok,
	]);
	assert_eq!(game.players[0].queue[0], Tiles::new(Tile::Green, 1));

	game.click(TilePath::Select(TileSource{source: 2, kind: Tile::White}));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Select {
			source: 2,
			tiles: Tile::White * 1,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Queue(TileQueue{player: 0, row: 0}, 0));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Place {
			player: 0,
			source: 2,
			kind: Tile::White,
			queue: None,
			trash: 3,
		},
		Mutation::Ok,
	]);
	assert_eq!(game.players[0].queue[0], Tiles::new(Tile::Green, 1));
}

#[test]
fn test_trash_same_color() {
	use rand::SeedableRng;

	let mut game = Game::new(Config{
		players: 1,
		use_preset: false,
		rng: rand_pcg::Pcg64Mcg::seed_from_u64(0),
	});
	assert_eq!(game.mutations, vec![]);

	game.click(TilePath::Select(TileSource{source: 3, kind: Tile::Green}));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Select {
			source: 3,
			tiles: Tile::Green * 3,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Queue(TileQueue{player: 0, row: 0}, 0));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Place {
			player: 0,
			source: 3,
			kind: Tile::Green,
			queue: Some(Queue{
				row: 0,
				tiles: Tile::Green*1,
			}),
			trash: 2,
		},
		Mutation::Ok,
	]);
	assert_eq!(game.players[0].queue[0], Tiles::new(Tile::Green, 1));

	game.click(TilePath::Select(TileSource{source: 2, kind: Tile::Green}));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Select {
			source: 2,
			tiles: Tile::Green * 1,
		},
		Mutation::Ok,
	]);

	game.click(TilePath::Queue(TileQueue{player: 0, row: 0}, 0));
	assert_eq!(game.mutations.split_off(0), vec![
		Mutation::Place {
			player: 0,
			source: 2,
			kind: Tile::Green,
			queue: None,
			trash: 3,
		},
		Mutation::Ok,
	]);
	assert_eq!(game.players[0].queue[0], Tiles::new(Tile::Green, 1));
}

#[test]
fn test_player_score(){
	let mut player = Player::new();
	assert_eq!(player.tile_score(0, 4), 1);

	player.board[0][4] = Some(Tile::Black);
	assert_eq!(player.tile_score(1, 4), 2);

	player.board[1][4] = Some(Tile::Blue);
	assert_eq!(player.tile_score(2, 4), 3);
}


#[test]
fn test_player_score_corners(){
	let player = Player::new();
	assert_eq!(player.tile_score(0, 0), 1);
	assert_eq!(player.tile_score(4, 4), 1);
	assert_eq!(player.tile_score(0, 4), 1);
	assert_eq!(player.tile_score(4, 0), 1);
}

#[test]
fn test_player_score_sequence(){
	let mut player = Player::new();
	player.board[0][4] = Some(Tile::Black);
	player.board[1][4] = Some(Tile::Blue);
	assert_eq!(player.tile_score(2, 4), 3);

	player.board[2][4] = Some(Tile::Red);
	assert_eq!(player.tile_score(3, 4), 4);
}


#[test]
fn test_player_score_middle_row(){
	let mut player = Player::new();
	player.board[1][4] = Some(Tile::Green);
	player.board[3][4] = Some(Tile::Red);
	assert_eq!(player.tile_score(2, 4), 3);
}

#[test]
fn test_player_score_middle_with_full_roll() {
	let mut player = Player::new();
	player.board[0][4] = Some(Tile::Blue);
	player.board[1][4] = Some(Tile::Green);
	player.board[3][4] = Some(Tile::Red);
	player.board[4][4] = Some(Tile::Black);
	assert_eq!(player.tile_score(2, 4), 5);
}

#[test]
fn test_player_score_cross() {
	let mut player = Player::new();
	player.board[1][2] = Some(Tile::Green);
	player.board[2][1] = Some(Tile::Black);
	player.board[2][3] = Some(Tile::Blue);
	player.board[3][2] = Some(Tile::Red);
	assert_eq!(player.tile_score(2, 2), 6);
}

#[test]
fn test_player_score_t_shape() {
	let mut player = Player::new();
	player.board[1][2] = Some(Tile::Green);
	player.board[2][3] = Some(Tile::Blue);
	player.board[3][2] = Some(Tile::Red);
	assert_eq!(player.tile_score(2, 2), 5);
}

#[test]
fn test_player_score_l_shape() {
	let mut player = Player::new();
	player.board[0][2] = Some(Tile::Red);
	player.board[1][2] = Some(Tile::Green);
	player.board[2][3] = Some(Tile::Blue);
	assert_eq!(player.tile_score(2, 2), 5);
}

#[test]
fn test_player_score_surrounding_shape() {
	let mut player = Player::new();
	player.board[0][1] = Some(Tile::Red);
	player.board[0][3] = Some(Tile::Red);
	player.board[2][0] = Some(Tile::Red);
	assert_eq!(player.tile_score(0, 0), 2);
}

#[test]
fn test_preset() {
	let mut player = Player::new();
	player.preset = Some(default_preset());
	assert_eq!(vec![0], player.can_score_tile(0, Tile::Red).collect::<Vec<_>>());
	assert_eq!(vec![4], player.can_score_tile(1, Tile::Red).collect::<Vec<_>>());
	assert_eq!(vec![1], player.can_score_tile(4, Tile::Red).collect::<Vec<_>>());
	assert_eq!(vec![2], player.can_score_tile(2, Tile::White).collect::<Vec<_>>());
}

#[test]
fn test_finish_row() {
	let mut player = Player::new();
	player.board = parse_board(r"
		.GUBW
		.UBRG
		..WG.
		.....
	");
	assert_eq!(vec![0, 1, 2, 4], player.can_score_tile(4, Tile::Red).collect::<Vec<_>>());
}

#[test]
fn test_finish_row_2() {
	let mut player = Player::new();
	player.board = parse_board(r"
		..U..
		..G..
		.....
		.....
	");
	assert_eq!(vec![0, 1, 2, 3, 4], player.can_score_tile(2, Tile::Red).collect::<Vec<_>>());
}
