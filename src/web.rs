use wasm_bindgen::JsCast;

#[cfg(target_arch="wasm32")]
use crate::eprintln;

#[derive(Debug)]
struct UiPlayer {
	board: web_sys::HtmlElement,
	score: web_sys::HtmlElement,
	trash: web_sys::HtmlElement,
	queues: Vec<Vec<web_sys::HtmlElement>>,
}

#[derive(Debug)]
pub struct UiGame {
	pub game: crate::Game,
	previous_player: u8,

	html: web_sys::HtmlElement,
	remove_fn: js_sys::Function,

	first_player: web_sys::HtmlElement,
	selected_tile: web_sys::HtmlElement,
	sources: Vec<Vec<(crate::Tile, web_sys::HtmlElement)>>,
	players: Vec<UiPlayer>,
}

impl UiGame {
	pub fn new(config: crate::Config<rand_pcg::Pcg32>, html: web_sys::HtmlElement) -> Self {
		let game = crate::Game::new(config);

		html.class_list().add_1("tiles").unwrap();

		let mut game_ui = UiGame {
			first_player: html.clone(),
			selected_tile: html.clone(),
			sources: game.sources.iter().map(|_| Vec::new()).collect(),
			players: game.players.iter().map(|p| UiPlayer{
				board: html.clone(),
				score: html.clone(),
				trash: html.clone(),
				queues: p.queue.iter().map(|_| Vec::new()).collect(),
			}).collect(),

			game,
			previous_player: 0,

			html,
			remove_fn: js_sys::Function::new_with_args("e", "e.target.remove()"),
		};
		game_ui.init();
		game_ui
	}

	fn init(&mut self) {
		self.html.set_inner_text("");

		self.update_sources();

		let tile = self.create_tile();
		tile.class_list().add_1("tile-first").unwrap();
		tile.set_inner_html("<span>F</span>");
		self.first_player = tile;
		self.update_first_player();

		for (i, player) in self.game.players.iter().enumerate() {
			self.html.insert_adjacent_html("beforeend", &format!(
				"<div class='player' style='top:{}em'><h2>Player {}</h2></div>",
				self.player_y(i as u8),
				i + 1)).unwrap();
			self.players[i].board = self.html.last_element_child().unwrap().unchecked_into();

			self.html.insert_adjacent_html("beforeend", &format!(
				"<div class='score' style='top:{}em'><span>Score: </span><span class=n>{}</span></div>",
				self.player_y(i as u8),
				player.score)).unwrap();
			self.players[i].score = self.html
				.last_element_child().unwrap()
				.last_element_child().unwrap()
				.unchecked_into();

			self.html.insert_adjacent_html("beforeend", &format!(
				"<div class='trash' style='top:{}em'><span>Trash: </span><span class=n>{}</span></div>",
				self.player_y(i as u8),
				player.trash)).unwrap();
			self.players[i].trash = self.html
				.last_element_child().unwrap()
				.last_element_child().unwrap()
				.unchecked_into();

			for &row in crate::ROWS {
				let board_row = player.board_row(row);
				for (col, _) in board_row.iter().enumerate() {
					let el = self.create_tile();
					el.class_list().add_2("cell", "empty").unwrap();
					self.update_tile(&el, crate::TilePath::Score(crate::TileBoard{player: i as u8, row, col: col as u8}));
				}

				for col in 0..crate::queue_size(row) {
					let el = self.create_tile();
					el.class_list().add_2("queue", "empty").unwrap();
					self.update_tile(&el, crate::TilePath::Queue(crate::TileQueue{player: i as u8, row}, col));
				}

				for (col, cell) in board_row.iter().enumerate() {
					if let Some(tile) = cell {
						let el = self.create_tile();
						el.class_list().add_1(tile.id()).unwrap();
						self.update_tile(&el, crate::TilePath::Score(crate::TileBoard{
							player: i as u8,
							row,
							col: col as u8,
						}));
					}
				}

				for el in self.players[i].queues[row as usize].drain(..) {
					el.remove();
				}

				let slot = player.queue_row(row);
				let slot_len = slot.as_ref().map_or(0, |t| t.len()).into();
				self.players[i].queues[row as usize].clear();
				self.players[i].queues[row as usize].reserve(slot_len);
				for (col, tile) in slot.iter().map(|tiles| tiles.iter()).flatten().enumerate() {
					let el = self.create_tile();
					el.class_list().add_1(tile.id()).unwrap();
					self.update_tile(&el,
						crate::TilePath::Queue(crate::TileQueue{
							player: i as u8,
							row,
						},
						col as u8));
					self.players[i].queues[row as usize].push(el);
				}
			}
		}

		self.update_current_player();
	}

	pub fn set_game(&mut self, game: crate::Game<rand_pcg::Pcg32>) {
		self.game = game;
		self.init();
		self.update_sources();
	}

	fn create_tile(&self) -> web_sys::HtmlElement {
		self.html.insert_adjacent_html("beforeend", "<div class=tile />").unwrap();
		self.html.last_element_child().unwrap().unchecked_into()
	}

	fn create_toast(&self, kind: &str, msg: &str) {
		self.html.insert_adjacent_html("beforeend", "<div class=toast />").unwrap();
		let el = self.html.last_element_child().unwrap().unchecked_into::<web_sys::HtmlElement>();
		el.class_list().add_1(kind).unwrap();
		el.set_inner_text(msg);
		web_sys::window().unwrap()
			.set_timeout_with_callback_and_timeout_and_arguments_0(
				&wasm_bindgen::closure::Closure::once_into_js(move || {
					el.remove();
				}).unchecked_into(), 5000)
				.unwrap();
	}

	fn player_y(&self, player: u8) -> f32 {
		let sources_height = self.game.sources.len() as f32 + 0.4;
		let player_height = self.game.players[0].board.len() as f32 + 1.4;

		sources_height + player_height * player as f32
	}

	fn update_tile(&self, tile: &web_sys::HtmlElement, path: crate::TilePath) {
		tile.dataset().set("id", &serde_json::to_string(&path).unwrap()).unwrap();

		let board_width = self.game.players[0].board[0].len() as f32 + 0.4;

		let (row, col) = match path {
			crate::TilePath::Select(crate::TileSource{source, kind}) => {
				let col = self.sources[source as usize].iter()
					.position(|(k, _)| k == &kind)
					.unwrap();
				(source as f32, col as f32 + 1.0)
			},
			crate::TilePath::Score(crate::TileBoard{row, col, player}) => {
				(self.player_y(player) + 1.0 + row as f32, col as f32)
			}
			crate::TilePath::Queue(crate::TileQueue{player, row}, col) => {
				(self.player_y(player) + 1.0 + row as f32, board_width + col as f32)
			}
		};
		let style = tile.style();
		style.set_property("left", &format!("{}em", col)).unwrap();
		style.set_property("top", &format!("{}em", row)).unwrap();
	}
	
	fn update_first_player(&mut self) {
		if let Some(player) = self.game.first_player {
			self.first_player.style().set_property(
				"top",
				&format!("{}em", self.player_y(player))).unwrap();
		} else {
			self.first_player.style().set_property("top", "").unwrap();
		}
	}

	fn update_sources(&mut self) {
		for (row, source) in self.game.sources.iter().enumerate() {
			for (_, el) in self.sources[row].drain(..) {
				el.remove();
			}

			for tiles in source.iter_non_empty() {
				let tile = self.create_tile();
				tile.class_list().add_1(tiles.kind().id()).unwrap();
				tile.set_inner_html(&format!("<span>{}</span>", tiles.len()));

				self.sources[row].push((tiles.kind(), tile.clone()));
				self.update_tile(&tile,
					crate::TilePath::Select(crate::TileSource{
						source: row as u8,
						kind: tiles.kind(),
					}));

				if let crate::State::Queuing{source, kind} = self.game.state {
					if source == row as u8 && kind == tiles.kind() {
						tile.class_list().add_1("active").unwrap();
						self.selected_tile = tile;
					}
				}
			}
		}
	}

	fn update_current_player(&mut self) {
		eprintln!("cur: {}, prev: {}", self.game.current_player, self.previous_player);
		self.players[self.previous_player as usize].board.class_list().remove_1("active").unwrap();
		self.previous_player = self.game.current_player;
		self.players[self.previous_player as usize].board.class_list().add_1("active").unwrap();
	}

	pub fn apply(&mut self, mutation: crate::Mutation) {
		self.game.mutate(mutation.clone());
		self.update(mutation);
	}

	pub fn update(&mut self, mutation: crate::Mutation) {
		eprintln!("{:?}", mutation);

		match mutation {
			crate::Mutation::Ok => {},
			crate::Mutation::Err(e) => {
				self.create_toast("err", &e);
			},
			crate::Mutation::NewRound{sources: _, scores} => {
				self.update_sources();
				self.update_first_player();
				self.update_current_player();

				for (player, score) in self.players.iter().zip(scores) {
					player.score.set_inner_text(&score.to_string());
					player.trash.set_inner_text("0");
				}
			}
			crate::Mutation::Select{source, tiles} => {
				let tile = &self.sources[source as usize].iter()
					.find(|(kind, _)| *kind == tiles.kind())
					.unwrap()
					.1;

				tile.class_list().add_1("active").unwrap();
				self.selected_tile.class_list().remove_1("active").unwrap();
				self.selected_tile = tile.clone();
			}
			crate::Mutation::Place{player, source: source_id, kind, queue, trash} => {
				let source = &mut self.sources[source_id as usize];
				let i = source.iter()
					.position(|(k, _)| *k == kind)
					.unwrap();

				let removed = source.swap_remove(i).1;
				removed.class_list().remove_1("active").unwrap();
				removed.set_inner_text("");

				if let Some(crate::Queue{row, tiles}) = queue {
					let queue = &mut self.players[player as usize].queues[row as usize];
					queue.reserve(crate::queue_size(row) as usize);
					let existing = queue.len();
					let new = tiles.len() - existing as u8;

					for _ in existing as u8+1..tiles.len() {
						let tile = removed.clone_node_with_deep(true).unwrap()
							.unchecked_into::<web_sys::HtmlElement>();
						self.html.append_child(&tile).unwrap();
						queue.push(tile);
					}

					if new > 1 {
						// Force layout so that transitions apply.
						removed.offset_left();
					}

					queue.push(removed);

					let queue = &self.players[player as usize].queues[row as usize];
					for (col, tile) in queue.iter().enumerate().skip(existing) {
						self.update_tile(&tile, crate::TilePath::Queue(crate::TileQueue{
							player,
							row,
						}, col as u8));
					}
				} else {
					removed.remove();
				}

				self.players[player as usize].trash.set_inner_text(&trash.to_string());

				let source = &mut self.sources[source_id as usize];
				if source_id != 0 {
					for (kind, el) in source.split_off(0) {
						let col = self.sources[0].iter()
							.position(|(k, _)| *k == kind);
						match col {
							Some(col) => {
								el.add_event_listener_with_callback(
									"transitionend", &self.remove_fn).unwrap();

								let el = &self.sources[0][col].1;
								let text = el.last_element_child().unwrap();
								text.set_inner_html(&self.game.sources[0][kind].to_string());
							}
							None => {
								el.style().set_property("z-index", "10").unwrap();
								self.sources[0].push((kind, el.clone()));
							}
						}
						self.update_tile(&el, crate::TilePath::Select(crate::TileSource{source: 0, kind}));
					}
				} else if let Some((kind, el)) = source.get(i).cloned() {
						self.update_tile(&el, crate::TilePath::Select(crate::TileSource{source: 0, kind}));
				}

				if source_id == 0 {
					self.update_first_player();
				}
				self.update_current_player();
			}
			crate::Mutation::Score{queue, col, score} => {
				let player = &mut self.players[queue.player as usize];
				player.score.set_inner_text(&score.to_string());
				for el in player.queues[queue.row as usize].split_off(0) {
					self.update_tile(&el, crate::TilePath::Score(crate::TileBoard{
						player: queue.player,
						row: queue.row,
						col,
					}));
				}
				self.update_current_player();
			}
		}
	}
}
