use wasm_bindgen::JsCast;
use wasm_bindgen::prelude::*;

use crate::eprintln;

#[derive(Debug,serde_derive::Deserialize,serde_derive::Serialize)]
struct Params {
	room: Option<String>,
}

impl Params {
	fn get() -> Self {
		let window = web_sys::window().unwrap();
		let location = window.location();
		let mut hash = location.hash().unwrap();
		if !hash.is_empty() {
			hash.remove(0);
		}
		serde_urlencoded::from_str(&hash).unwrap()
	}

	fn set(&self) {
		let window = web_sys::window().unwrap();
		let location = window.location();
		location.set_hash(&serde_urlencoded::to_string(self).unwrap()).unwrap();
	}
}

#[wasm_bindgen]
#[derive(Debug)]
pub struct Controller(std::rc::Rc<std::cell::RefCell<Data>>);

#[derive(Debug)]
struct Data {
	html: web_sys::HtmlElement,
	click_cb: Closure<dyn Fn(web_sys::MouseEvent)>,
	state: State,
	api_url: String,
}

#[derive(Debug)]
enum State {
	Menu,
	Config(bool),
	Join,
	Connecting(Connection),
	Playing(Game),
}

#[wasm_bindgen]
impl Controller {
	pub fn new(html: web_sys::HtmlElement, api_url: String) -> Controller {
		let params = Params::get();

		html.class_list().add_1("tiles").unwrap();

		let mut controller = Controller(std::rc::Rc::new(Data{
			html: html.clone(),
			click_cb: Closure::wrap(Box::new(|_| {})),
			state: State::Menu,
			api_url,
		}.into()));

		let weak = std::rc::Rc::downgrade(&controller.0);
		let click_cb = Closure::wrap(Box::new(move |e: web_sys::MouseEvent| {
			let id = e.target()
				.and_then(|t| t.dyn_into::<web_sys::HtmlElement>().ok())
				.and_then(|t| t.dataset().get("id"));
			let id = match id {
				Some(id) => id,
				None => return,
			};
			Controller(weak.upgrade().unwrap()).click(&id);
		}) as Box<dyn Fn(web_sys::MouseEvent)>);
		html.set_onclick(Some(click_cb.as_ref().unchecked_ref()));
		controller.0.borrow_mut().click_cb = click_cb;

		if let Some(room) = &params.room {
			controller.connect_room(room);
		}

		controller
	}

	fn click(&mut self, target: &str) {
		let mut this = self.0.borrow_mut();
		match this.state {
			State::Menu => {
				std::mem::drop(this);
				match target {
					"local" => self.render_config(false),
					"new" => self.render_config(true),
					"join" => self.render_join(),
					_ => unreachable!("Unexpected option {:?}", target),
				}
			}
			State::Join => {
				let room = this.html.query_selector(".r").unwrap().unwrap()
					.unchecked_into::<web_sys::HtmlInputElement>()
					.value();

				std::mem::drop(this);
				self.connect_room(&room);
			},
			State::Config(network) => if target == "s" {
				let mut config = crate::Config::default();
				config.players = this.html.query_selector(".n").unwrap().unwrap()
					.unchecked_into::<web_sys::HtmlInputElement>()
					.value()
					.parse().unwrap();

				std::mem::drop(this);
				if network {
					self.connect_room("test");
				} else {
					self.start_game(config)
				}
			},
			State::Connecting(..) => eprintln!("Click while connecting."),
			State::Playing(ref mut game) => game.click(target),
		}
	}

	fn connect_room(&mut self, room: &str) {
		let mut params = Params::get();
		params.room = Some(room.into());
		params.set();

		let weak = std::rc::Rc::downgrade(&self.0);
		let mut this = self.0.borrow_mut();

		let ws = web_sys::WebSocket::new_with_str(
			&format!("{}/game/{}", this.api_url, room),
			crate::net::PROTOCOL).unwrap();

		ws.set_binary_type(web_sys::BinaryType::Arraybuffer);

		let ws_cb = Closure::wrap(Box::new(move |e: web_sys::MessageEvent| {
			let msg = js_sys::Uint8Array::new(&e.data());
			let mut buf = vec![0; msg.length() as usize];
			msg.copy_to(&mut buf);
			let msg = bincode::deserialize(&buf).unwrap();

			let this = weak.upgrade().unwrap();
			let mut this = this.borrow_mut();

			if let State::Connecting(_) = this.state {
				let state = std::mem::replace(&mut this.state, State::Menu);
				let connection = match state {
					State::Connecting(connection) => connection,
					_ => unreachable!(),
				};

				let ui = crate::web::UiGame::new(
					Default::default(),
					this.html.clone());
				this.state = State::Playing(Game{
					connection: Some(connection),
					ui,
					expected_mutations: Vec::new(),
				})
			}

			let gamestate = match this.state {
				State::Playing(ref mut game) => game,
				_ => return,
			};

			match msg {
				crate::net::EventServer::New(game) => {
					gamestate.ui.set_game(game);
				}
				crate::net::EventServer::Mutation(m) => {
					if gamestate.expected_mutations.is_empty() {
						debug_assert_eq!(gamestate.ui.game.mutations, &[]);
						gamestate.ui.apply(m);
						gamestate.ui.game.mutations.clear();
					} else {
						let expected = gamestate.expected_mutations.remove(0);
						if m != expected {
							eprintln!("Desync:\npredicted: {:#?}\nactual: {:#?}",  expected, m);
							let mut msg = bincode::serialize(&crate::net::EventClient::Desync).unwrap();
							gamestate.connection.as_ref().unwrap()
								.ws.send_with_u8_array(&mut msg).unwrap();
						}
					}
				}
			}
		}) as Box<dyn Fn(web_sys::MessageEvent)>);

		ws.set_onmessage(Some(ws_cb.as_ref().unchecked_ref()));

		let connection = Connection{ ws, ws_cb };

		this.state = State::Connecting(connection);
		this.html.set_inner_html("<p>Connecting...</p>");
	}

	fn render_join(&self) {
		let mut this = self.0.borrow_mut();
		this.state = State::Join;
		this.html.set_inner_html("\
			<label>Room Code: <input class=r type=text /></label>\
			<button data-id=>Join</button>\
		");
	}

	fn render_config(&self, network: bool) {
		let mut this = self.0.borrow_mut();
		this.state = State::Config(network);
		this.html.set_inner_html("\
			<label>Players: <input class=n type=number value=2 min=2 /></label>\
			<button data-id=s>Start</button>\
		");
	}

	fn start_game(&self, config: crate::Config<rand_pcg::Pcg32>) {
		let mut this = self.0.borrow_mut();
		let ui = crate::web::UiGame::new(config, this.html.clone());
		this.state = State::Playing(Game{
			connection: None,
			ui,
			expected_mutations: Vec::new(),
		})
	}

	pub fn dump(&self) -> String {
		format!("{:#?}", self)
	}
}

#[derive(Debug)]
struct Connection {
	ws: web_sys::WebSocket,

	#[allow(unused)]
	ws_cb: Closure<dyn Fn(web_sys::MessageEvent)>,
}

#[derive(Debug)]
struct Game {
	connection: Option<Connection>,
	ui: crate::web::UiGame,
	expected_mutations: Vec<crate::Mutation>,
}

impl Game {
	fn click(&mut self, target: &str) {
		let target: crate::TilePath = serde_json::from_str(target).unwrap();

		if let Some(connection) = &self.connection {
			let event = crate::net::EventClient::Click(target.clone());
			let mut msg = bincode::serialize(&event).unwrap();
			connection.ws.send_with_u8_array(&mut msg).unwrap();
		}

		self.ui.game.click(target);

		for mutation in self.ui.game.mutations.split_off(0) {
			self.ui.update(mutation.clone());
			self.expected_mutations.push(mutation);
		}
	}
}

