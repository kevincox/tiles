pub mod controller;
mod game;
pub mod net;
mod panic;
mod web;

use game::*;

pub use game::{
	Config,
	Game,
	TILES,
	TileBoard,
	TilePath,
	TileQueue,
	TileSource,
};

pub type Prng = rand_pcg::Pcg32;

#[cfg(target_arch="wasm32")]
#[macro_export]
macro_rules! eprintln {
	($($arg:tt)*) => (web_sys::console::log_1(&format!($($arg)*).into()));
}

#[cfg(not(target_arch="wasm32"))]
pub use std::eprintln;
